<html>
<head>
	<title>Prasus Constructions</title>
	<link rel="stylesheet" type="text/css" href="css/app.css">
</head>
<body>
	
	<div class="contact_title">
		<h1>Prasus Constructions - Building the Future</h1>
		<h2>We are always ready to serve you!</h2>
	</div>
	<div class="contact_form">
		<form id="contact_form" method="post" action="functions/mail.php">
			<input type="text" name="name" class="form_control" placeholder="Your Name" required>
			<br>
			<input type="email" name="email" class="form_control" placeholder="Your Email" required><br>
			<textarea name="message" class="form_control" placeholder="Message" row="4" required></textarea><br>
			<input type="submit" name="" class="form_control submit" value="SEND MESSAGE">
		</form>
	</div>

</body>

</html>
